<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ajuan_Dana extends Model
{
    use HasFactory;
    protected $table = 'ajuan_dana';
    protected $primaryKey = 'id_ajuan_dana';
    protected $fillable = [
        'id_departemen',
        'keterangan',
        'nominal',
        'nota',
        'status_ajuan_dana',
        'tanggal_ajuan',
    ];
}

?>