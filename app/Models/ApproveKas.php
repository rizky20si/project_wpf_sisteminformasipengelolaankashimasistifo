<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApproveKas extends Model
{
    use HasFactory;
    protected $table = 'approve_kas';
    protected $primaryKey = 'id_approve_kas';
    protected $fillable = [
        'id_anggota',
        'id_kas',
        'status_approve_kas',
        'tgl_approve_kas'
    ];
}
