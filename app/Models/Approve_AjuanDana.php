<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Approve_AjuanDana extends Model
{
    use HasFactory;
    protected $table = 'approve_ajuan';
    protected $primaryKey = 'id_approve_ajuan';
    protected $fillable = [
        'id_ajuan_dana',
        'status_approve_ajuan',
        'tanggal_approve',
    ];
}

?>