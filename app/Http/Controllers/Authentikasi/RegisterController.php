<?php

namespace App\Http\Controllers\Authentikasi;

use App\Models\Anggota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'nim' => 'required',
            'password' => 'required',
            'repassword' => 'required|same:password',
            'angkatan' => 'required',
        ]);
        if(!$validator->fails()){
            
            $anggota = Anggota::create([
                'nama_departemen' => $request->nama_departemen,
                'password' => $request->password,
                'kode_akses' => $request->kode_akses,
    
            ]);
            return $validated_data;
        }
    }
    
}
