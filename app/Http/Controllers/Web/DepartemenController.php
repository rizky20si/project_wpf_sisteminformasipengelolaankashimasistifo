<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Departemen;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class DepartemenController extends Controller
{
    public function index()
    {   
        $departemens = DB::select('select * from departemen');
        return view('departemen.layouts.list', ['departemens' => $departemens]);
    }

    public function loginDepartemen()
    {
        $departemens = DB::select('select * from departemen');
        return view('departemen.auth.login', ['departemens'=>$departemens]);
    }

    public function loginDepartemenProses(Request $request){
        $nama_departemen = $request->nama_departemen;
        $password = Hash::make($request->password);

        $data = Departemen::where(['nama_departemen'=>$nama_departemen])->first();

        if($data){
            Session::put('role', $data['nama_departemen']);
            return redirect('/departemen');
        }else{
            Session::flash('failed', 'Email dan Password anda salah');
            return $data;
        }
    }

    public function logout(Request $request){
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->flush();

        return redirect('/departemen/login');
   }
}
