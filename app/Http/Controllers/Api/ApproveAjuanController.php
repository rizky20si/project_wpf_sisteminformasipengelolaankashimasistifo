<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Approve_AjuanDana;
use App\Models\Ajuan_Dana;
use App\Models\Departemen;
use App\Models\Keuangan;
use App\Http\Resources\DriverResource;
use Illuminate\Support\Facades\Validator;
use DB;

class ApproveAjuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $approveAjuan = Approve_AjuanDana::latest()->paginate(5);
        return new DriverResource(true,'List Ajuan', $approveAjuan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $approveAjuan =  Approve_AjuanDana::find($id);
        $ajuanDana = Ajuan_Dana::find($approveAjuan->id_ajuan_dana);
        $departemen = Departemen::find($ajuanDana->id_departemen);
        $MasukAtribut = Validator::make($request->all(), [
            'status_approve_ajuan' => 'required'
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        $approveAjuan->update([
            'status_approve_ajuan'=>$request->status_approve_ajuan
        ]);
        if($approveAjuan->status_approve_ajuan == 1){
            $keuangan = Keuangan::create([
                'nominal_keuangan' => $ajuanDana->nominal,
                'deskripsi' => 'Pengajuan Dana dari '.$departemen->nama_departemen,
                'status' => 'Uang Keluar',
                'bukti' => $ajuanDana->nota
            ]);
        }
        return new DriverResource(true, 'Data Approve Ajuan Dana Berhasil Diubah!', $approveAjuan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
