<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Keuangan;
use App\Http\Resources\DriverResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use DB;

class KeuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keuangan = Keuangan::latest()->paginate(5);
        return new DriverResource(true,'List Keuangan', $keuangan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $MasukAtribut = Validator::make($request->all(), [
            'nominal_keuangan' => 'required',
            'deskripsi' => 'required',
            'status' => 'required',
            'bukti' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        $bukti = $request->file('bukti');
        $filename = $bukti->hashName();
        $bukti->storeAs('public/Keuangan', $filename);

        $keuangan = Keuangan::create([
            'nominal_keuangan' => $request->nominal_keuangan,
            'deskripsi' => $request->deskripsi,
            'status' => $request->status,
            'bukti' => $filename
        ]);

        return new DriverResource(true, 'Data Keuangan Berhasil Ditambahkan!', $keuangan);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $keuangan =  Keuangan::find($id);
        $MasukAtribut = Validator::make($request->all(), [
            'nominal_keuangan' => 'required',
            'deskripsi' => 'required',
            'status' => 'required'
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        if($request->hasFile('bukti')){
            $bukti = $request->file('bukti');
            $filename = $bukti->hashName();
            $bukti->storeAs('public/Keuangan', $filename);

            Storage::delete('public/Keuangan/'.$keuangan->bukti);

            $keuangan->update([
                'nominal_keuangan' => $request->nominal_keuangan,
                'deskripsi' => $request->deskripsi,
                'status' => $request->status,
                'bukti' => $filename
            ]);
        }else{
            $keuangan->update([
                'nominal_keuangan' => $request->nominal_keuangan,
                'deskripsi' => $request->deskripsi,
                'status' => $request->status
            ]);
        }
        

        return new DriverResource(true, 'Data Keuangan Berhasil Diubah!', $keuangan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $keuangan =  Keuangan::find($id);
        Storage::delete('public/Keuangan/'.$keuangan->bukti);
        $keuangan->delete();
        return new DriverResource(true, 'Data Keuangan Berhasil Dihapus!', $keuangan);
    }
}
