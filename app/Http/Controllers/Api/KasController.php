<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kas;
use App\Models\ApproveKas;
use App\Http\Resources\DriverResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use DB;

class KasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kas = Kas::latest()->paginate(5);
        return new DriverResource(true,'List Anggota', $kas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $MasukAtribut = Validator::make($request->all(), [
            'nominal' => 'required',
            'id_anggota' => 'required',
            'keterangan' => 'required',
            'bulan' => 'required',
            'bukti_bayar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        $bukti_bayar = $request->file('bukti_bayar');
        $filename = $bukti_bayar->hashName();
        $bukti_bayar->storeAs('public/Kas', $filename);

        $kas = Kas::create([
            'nominal' => $request->nominal,
            'id_anggota' => $request->id_anggota,
            'keterangan' => $request->keterangan,
            'bulan' => $request->bulan,
            'bukti_bayar' => $filename
        ]);
        $id_kas = DB::select("select id_kas from  kas where bukti_bayar='".$filename."'");
        $approve_kas = ApproveKas::create([
            'id_kas' => $id_kas[0]->id_kas,
            'id_anggota' => $request->id_anggota,
            'status_approve_kas' =>'0',
        ]);

        return new DriverResource(true, 'Data Kas Berhasil Ditambahkan!', $kas);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kas = Kas::find($id);
        return new DriverResource(true, 'Data Anggota', $kas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kas =  Kas::find($id);
        $MasukAtribut = Validator::make($request->all(), [
            'nominal' => 'required',
            'keterangan' => 'required',
            'bulan' => 'required'
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        if($request->hasFile('bukti_bayar')){
            $bukti_bayar = $request->file('bukti_bayar');
            $filename = $bukti_bayar->hashName();
            $bukti_bayar->storeAs('public/Kas/', $filename);

            Storage::delete('public/Kas/'.$kas->bukti_bayar);

            $kas->update([
                'nominal'=>$request->nominal,
                'keterangan'=>$request->keterangan,
                'bulan'=>$request->bulan,
                'bukti_bayar'=>$filename
            ]);
        }else{
            $kas->update([
                'nominal'=>$request->nominal,
                'keterangan'=>$request->keterangan,
                'bulan'=>$request->bulan
            ]);
        }

        return new DriverResource(true, 'Data Kas Berhasil Diubah!', $kas);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
