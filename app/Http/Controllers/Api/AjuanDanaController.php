<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ajuan_Dana;
use App\Models\Approve_AjuanDana;
use App\Http\Resources\DriverResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use DB;

class AjuanDanaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ajuanDana = Ajuan_Dana::latest()->paginate(5);
        return new DriverResource(true,'List Ajuan Dana', $ajuanDana);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $MasukAtribut = Validator::make($request->all(), [
            'id_departemen' => 'required',
            'keterangan' => 'required',
            'nominal' => 'required',
            'nota' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status_ajuan_dana' => 'required',
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        $image = $request->file('nota');
        $filename = $image->hashName();
        $image -> storeAs('public/AjuanDana', $filename);

        $ajuandana = Ajuan_Dana::create([
            'id_departemen' => $request->id_departemen,
            'keterangan' => $request->keterangan,
            'nominal' => $request->nominal,
            'nota' => $filename,
            'status_ajuan_dana' => $request->status_ajuan_dana,
        ]);
        $id_ajuan_dana = DB::select("select id_ajuan_dana from ajuan_dana where nota='".$filename."'");
        $approve_ajuan_dana = Approve_AjuanDana::create([
            'id_ajuan_dana' => $id_ajuan_dana[0]->id_ajuan_dana,
            'status_approve_ajuan' => '0'
        ]);
        return new DriverResource(true, 'Data Ajuan Dana Ditambahkan!', $ajuandana);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ajuandana = Ajuan_Dana::find($id);
        return new DriverResource(true, 'Data Ajuan Dana', $ajuandana);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ajuan_Dana $ajuandana)
    {
        $MasukAtribut = Validator::make($request->all(), [
            'id_departemen' => 'required',
            'keterangan' => 'required',
            'nominal' => 'required',
            'status_ajuan_dana' => 'required',
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        if ($request->hasFile('nota')){

            $image = $request->file('nota');
            $image -> storeAs('public/AjuanDana', $image->hashName());

            Storage::delete('public/AjuanDana'.$ajuandana->image);

            $ajuandana->update([
                'id_departemen'=>$request->id_departemen,
                'keterangan'=>$request->keterangan,
                'nominal'=>$request->nominal,
                'nota'=>$image->hashName(),
                'status_ajuan_dana'=>$request->status_ajuan_dana,
            ]);
        } else {

            $ajuandana->update([
                'id_departemen'=>$request->id_departemen,
                'keterangan'=>$request->keterangan,
                'nominal'=>$request->nominal,
                'status_ajuan_dana'=>$request->status_ajuan_dana,
            ]);
        }

        return new DriverResource(true, 'Data Ajuan Dana berhasil diubah!', $ajuandana);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ajuandana = Ajuan_Dana::find($id);
        Storage::delete('public/AjuanDana/'.$ajuandana->image);
        $ajuandana->delete();
        return new DriverResource(true, 'Data Ajuan Dana berhasil dihapus!', $ajuandana);
    }
}