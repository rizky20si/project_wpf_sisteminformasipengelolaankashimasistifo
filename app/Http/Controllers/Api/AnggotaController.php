<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Anggota;
use App\Http\Resources\DriverResource;
use Illuminate\Support\Facades\Validator;
use DB;

class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota = Anggota::latest()->paginate(5);
        return new DriverResource(true,'List Anggota', $anggota);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response3
     * 
     */
    public function store(Request $request)
    {
        $MasukAtribut = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'nim' => 'required',
            'email' => 'required',
            'password' => 'required',
            'angkatan' => 'required',
            'google_id' => 'required'
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        $anggota = Anggota::create([
            'nama_lengkap' => $request->nama_lengkap,
            'nim' => $request->nim,
            'email' => $request->email,
            'password' => $request->password,
            'angkatan' => $request->angkatan,
            'google_id' => $request->google_id

        ]);

        return new DriverResource(true, 'Data Anggota Berhasil Ditambahkan!', $anggota);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anggota = Anggota::find($id);
        return new DriverResource(true, 'Data Anggota', $anggota);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $MasukAtribut = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'nim' => 'required',
            'email' => 'required',
            'password' => 'required',
            'angkatan' => 'required',
            'google_id' => 'required'
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        $anggota = DB::table('anggota')->where('id_anggota', $id)
        ->update(array('nama_lengkap'=>$request->nama_lengkap, 'nim'=>$request->nim, 'password'=>$request->password, 'angkatan'=>$request->angkatan, 'google_id'=>$request->google_id));

        return new DriverResource(true, 'Data Anggota berhasil diubah!', $anggota);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $anggota = Anggota::find($id);
        $anggota->delete();
        return new DriverResource(true, 'Data Departemen berhasil dihapus!', $anggota);
    }
}
