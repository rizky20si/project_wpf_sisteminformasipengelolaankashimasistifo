<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ApproveKas;
use App\Models\Keuangan;
use App\Models\Kas;
use App\Models\Anggota;
use App\Http\Resources\DriverResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use DB;

class ApproveKasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $approvekas = ApproveKas::latest()->paginate(5);
        return new DriverResource(true,'List Approve Kas', $approvekas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $approvekas =  ApproveKas::find($id);
        $kas = Kas::find($approvekas->id_kas);
        $anggota = Anggota::find($approvekas->id_anggota);
        $MasukAtribut = Validator::make($request->all(), [
            'status_approve_kas' => 'required'
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        $approvekas->update([
            'status_approve_kas'=>$request->status_approve_kas
        ]);
        if($approvekas->status_approve_kas == 1){
            $keuangan = Keuangan::create([
                'nominal_keuangan' => $kas->nominal,
                'deskripsi' => 'Uang Kas dari '.$anggota->nama_lengkap,
                'status' => 'Uang Masuk',
                'bukti' => $kas->bukti_bayar
            ]);
        }
        return new DriverResource(true, 'Data Approve Kas Berhasil Diubah!', $approvekas);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
