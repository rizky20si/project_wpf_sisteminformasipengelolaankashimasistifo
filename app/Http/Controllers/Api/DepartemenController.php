<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Departemen;
use App\Http\Resources\DriverResource;
use Illuminate\Support\Facades\Validator;
use DB;

class DepartemenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departemens = Departemen::latest()->paginate(5);
        return new DriverResource(true,'List Departemen', $departemens);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $MasukAtribut = Validator::make($request->all(), [
            'nama_departemen' => 'required',
            'password' => 'required',
            'status_akses' => 'required',
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        $departemen = Departemen::create([
            'nama_departemen' => $request->nama_departemen,
            'password' => $request->password,
            'status_akses' => $request->status_akses,

        ]);

        return new DriverResource(true, 'Data Departemen Berhasil Ditambahkan!', $departemen);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departemen = Departemen::find($id);
        return new DriverResource(true, 'Data Departemen', $departemen);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $departemen = Departemen::find($id);
        $MasukAtribut = Validator::make($request->all(), [
            'nama_departemen' => 'required',
            'password' => 'required',
            'status_akses' => 'required',
        ]);

        if ($MasukAtribut->fails()) {
            return response()->json($MasukAtribut->errors(), 422);
        }

        $departemen->update([
            'nama_departemen' => $request->nama_departemen,
            'password' => $request->password,
            'status_akses' => $request->status_akses,
        ]);

        return new DriverResource(true, 'Data Departemen berhasil diubah!', $departemen);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departemen = Departemen::find($id);
        $departemen->delete();
        return new DriverResource(true, 'Data Departemen berhasil dihapus!', $departemen);
    }
}
