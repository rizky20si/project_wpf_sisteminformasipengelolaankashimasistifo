<script>
//button create post event
$('body').on('click', '#btn-delete-departemen', function () {
    let id_departemen = $(this).data('id');
    let token = $("meta[name='csrf-token']").attr("nama_departemen");
        Swal.fire({
        title: 'Apakah Kamu Yakin?',
        text: "ingin menghapus data ini!",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'TIDAK',
        confirmButtonText: 'YA, HAPUS!'
    }).then((result) => {
        if (result.isConfirmed) {
            console.log(id_departemen);
            //fetch to delete data
            $.ajax({
                url: '{{url('api/departemen')}}/'+ id_departemen,
                type: "DELETE",
                cache: false,
                data: {
                    "_token": token
                },
                success:function(response){
                    //show success message

                    Swal.fire({

                        type: 'success',
                        icon: 'success',
                        title: `${response.message}`,
                        showConfirmButton: false,
                        timer: 3000

                    });
                    //remove post on table

                    $(`#index_${id_departemen}`).remove();

                }
            });

        }
    })
});
</script>
