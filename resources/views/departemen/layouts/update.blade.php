<!-- Modal -->

<div class="modal fade" id="modal-edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">EDIT DEPARTEMEN</h5>

<button type="button" class="close" data-dismiss="modal" aria-label="Close">

<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<form id="formData_edit" enctype="multipart/form-data" method="post">
<input type="hidden" id="id_departemen">
<div class="form-group d-flex flex-column mb-8 fv-row fv-plugins-icon-container">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Nama Departemen</span>
                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="Edit nama departemen" aria-label="Specify a target name for future usage and reference"></i>
            </label>
            <!--end::Label-->
            <input type="text" class="form-control form-control-solid" id="nama-departemen-edit" name="nama-departemen-edit">            
            <div class="alert alert-danger mt-2 d-none" role="alert" id="alert-nama-departemen-edit"></div>
    </div>
<div class="form-group d-flex flex-column mb-8 fv-row fv-plugins-icon-container">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
            <span class="required">Password</span>
            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="Edit password" aria-label="Specify a target name for future usage and reference"></i>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" id="password-edit" name="password-edit">            
        <div class="alert alert-danger mt-2 d-none" role="alert" id="alert-password-edit"></div>
</div>
<div class="form-group d-flex flex-column mb-8 fv-row fv-plugins-icon-container">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
            <span class="required">Status Akses</span>
            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="Edit status akses" aria-label="Specify a target name for future usage and reference"></i>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" id="status-akses-edit" name="status-akses-edit">            
        <div class="alert alert-danger mt-2 d-none" role="alert" id="alert-status-akses-edit"></div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
<button type="submit" class="btn btn-primary" id="update">UPDATE</button>
</div>
</form>
</div>
</div>
</div>
<script>
//button create post event
$('body').on('click', '#btn-edit-departemen', function () {
let id_departemen = $(this).data('id');
//fetch detail post with ajax
$.ajax({
url: '{{url('api/departemen')}}/'+id_departemen,
type: "GET",
cache: false,
success:function(response){
//fill data to form
$('#id_departemen').val(response.data.id_departemen);
$('#nama-departemen-edit').val(response.data.nama_departemen);
$('#password-edit').val(response.data.password);
$('#status-akses-edit').val(response.data.status_akses);
//open modal
$('#modal-edit').modal('show');
}
});
});
//action update post
$('#update').click(function(e) {
e.preventDefault();
e.stopPropagation();
let id_departemen=$('#id_departemen').val()
var form = new FormData();
form.append("nama_departemen",$('#nama-departemen-edit').val());
form.append("password", $('#password-edit').val());
form.append("status_akses", $('#status-akses-edit').val());
form.append("_method", "PUT");
//ajax
$.ajax({
url: '{{url('api/departemen')}}/'+id_departemen,
type: "POST",
data: form,
cache: false,
dataType: 'json',
processData: false,
contentType: false,
timeout: 0,
mimeType: "multipart/form-data",
headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
success:function(response){
//show success message
Swal.fire({
type: 'success',
icon: 'success',
title: `${response.message}`,
showConfirmButton: false,
timer: 3000
});

//data departemen
let departemen = `
<tr id="index_${ response.data.id_departemen }">
    <td>
        <div class="d-flex align-items-center">
            <div class="d-flex justify-content-start flex-column">
                <span class="text-dark fw-bolder text-hover-primary fs-6">${response.data.nama_departemen}</span>
            </div>
        </div>
    </td>
    <td>
        <span class="text-dark fw-bolder text-hover-primary d-block fs-6">${response.data.password}</span>
    </td>
    <td>
        <span class="text-dark fw-bolder text-hover-primary d-block fs-6">${response.data.status_akses}</span>
    </td>
    <td>
        <div class="d-flex">
            <a href="javascript:void(0)" id="btn-edit-departemen" data-id="${ response.data.id_departemen }" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                <!--begin::Svg Icon | path: icons/duotune/art/art005.svg-->
                <span class="svg-icon svg-icon-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black"></path>
                        <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black"></path>
                    </svg>
                </span>
                <!--end::Svg Icon-->
            </a>
            <a href="javascript:void(0)" id="btn-delete-departemen" data-id="${ response.data.id_departemen }" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm">
                <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                <span class="svg-icon svg-icon-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black"></path>
                        <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black"></path>
                        <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black"></path>
                    </svg>
                </span>
                <!--end::Svg Icon-->
            </a>
        </div>
    </td>
</tr>
`;

console.log(response.data)
//append to departemen data
$(`#index_${response.data.id_departemen}`).replaceWith(departemen);
//close modal
$('#modal-edit').modal('hide');

},
error:function(error){
if(error.responseJSON.nama_departemen[0]) {
//show alert
$('#alert-nama-departemen-edit').removeClass('d-none');
$('#alert-nama-departemen-edit').addClass('d-block');
//add message to alert
$('#alert-nama-departemen-edit').html(error.responseJSON.nama_departemen[0]);

}
if(error.responseJSON.password[0]) {
//show alert
$('#alert-password-edit').removeClass('d-none');
$('#alert-password-edit').addClass('d-block');
//add message to alert

$('#alert-password-edit').html(error.responseJSON.password[0]);

}
if(error.responseJSON.status_akses[0]) {
//show alert
$('#alert-status-akses-edit').removeClass('d-none');
$('#alert-status-akses-edit').addClass('d-block');
//add message to alert

$('#alert-status-akses-edit').html(error.responseJSON.status_akses[0]);

}
}
});
});
</script>