<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('/departemen', App\Http\Controllers\Api\DepartemenController::class);
Route::apiResource('/kas', App\Http\Controllers\Api\KasController::class);
Route::apiResource('/approvekas', App\Http\Controllers\Api\ApproveKasController::class);
Route::apiResource('/keuangan', App\Http\Controllers\Api\KeuanganController::class);
Route::apiResource('/anggota', App\Http\Controllers\Api\AnggotaController::class);
Route::apiResource('/approveajuandana', App\Http\Controllers\Api\ApproveAjuanController::class);
Route::apiResource('/ajuandana', App\Http\Controllers\Api\AjuanDanaController::class);