<?php

use Illuminate\Support\Facades\Route;

Route::get('/departemen/login', [App\Http\Controllers\Web\DepartemenController::class, 'loginDepartemen'])->name('departemen');
Route::post('/departemen/login', [App\Http\Controllers\Web\DepartemenController::class, 'loginDepartemenProses'])->name('departemenProses');

Route::get('/logout', [App\Http\Controllers\Web\DepartemenController::class, 'logout']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/login/auth/google', [App\Http\Controllers\Auth\GoogleController::class, 'redirectToGoogle']);
Route::get('/auth/google/callback', [App\Http\Controllers\Auth\GoogleController::class, 'handleGoogleCallback']);

Route::post('/auth/register', [App\Http\Controllers\Authentikasi\RegisterController::class, 'create']);
Route::post('/login', [App\Http\Controllers\LoginController::class])->name('login');
Route::get('/test', [App\Http\Controllers\TestController::class, 'test']);
Auth::routes();

Route::get('/departemen', [App\Http\Controllers\Web\DepartemenController::class, 'index'])->name('departemen.index');
